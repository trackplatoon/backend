@yield('title')

<br /><br /><br /><br />

@yield('meta_description')

<br /><br /><br /><br />

@yield('meta_keywords')

<br /><br /><br /><br />

@yield('content')
@yield('scripts')