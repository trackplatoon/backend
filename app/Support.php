<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    //
    public $timestamps = false;
    protected $table = 'support';
    protected $primaryKey = 'support_id';

    protected $fillable = ['post_id','user_id'];

    //A single support belongs to a single post
    public function postSupported()
    {
    	return $this->belongsTo('Post','post_id');
    }

    //A single support belongs to a single user
    public function userSupported()
    {
    	return $this->belongsTo('User','user_id');
    }


}
