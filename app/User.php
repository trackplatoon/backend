<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable 
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $table = 'user';
    protected $primaryKey = 'user_id';
    public $timestamps = false;

    protected $fillable = [
        'username', 'email', 'mobile_number','sex',
        'profile_image_uri','geolocation','cover_image_uri','password','address','street_line_1',
        'street_line_2','city','zip_code','region','country','birthdate','religion','high_school',
        'college','university','profession','blood'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    

    //An user can have many posts
    public function posts(){
        return $this-> hasMany('Post','user_id');
    }
    
    //An user has many comments
    public function comments()
    {
        return $this->hasMany('Comment','user_id');
    }

    //An user can support many contents
    public function support()
    {
        return $this->hasMany('Support','user_id');
    }

    //Follow-->Confused!
    public function followers()
    {
        return $this->hasMany('Follow','follower_id');
    }

    public function following()
    {
        return $this->hasMany('Follow','following_id');
    }

    //An user can send many notifications
    public function send(){
        return $this->hasMany('App\Notification','sender_id');
    }

    //An user can receive many notifications
    public function receive(){
        return $this->hasMany('App\Notification','receiver_id');
    }

   
}
