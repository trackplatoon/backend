<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    public $timestamps = false;
    protected $table = 'post';
    protected $primaryKey = 'post_id';

    protected $fillable  = ['user_id','incident_id','time_stamp','description',
    'attachment_uri','geolocation','image_uri','video_uri','is_anonymous'];



    //A post belongs to a user
    public function user()
    {
    	return $this->belongsTo('App\User','user_id');
    }

    //A post belongs a single incident type
    public function incidentToPost()
    {
    	return $this->belongsTo('App\Incident','incident_id');
    }

    //A post can have many comments
    public function commentsToPost()
    {
    	return $this->hasMany('App\Comment','post_id');
    }

    //A post can have many supports
    public function supportToPosts()
    {
    	return $this->hasMany('App\Support','post_id');
    }

    public function shareToPosts(){
        return $this->hasMany('App\Share','post_id');
    }

    //A post can have many notifications
    public function notify(){
        return $this->hasMany('App\Notification','post_id');
    }
}
