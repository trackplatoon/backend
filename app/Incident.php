<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Incident extends Model
{
    protected $table = 'incident_type';
    protected $primaryKey = 'incident_id';

    //A single incident type can be in many Posts
    public function incidentTypeInPosts()
    {
    	return $this->hasMany('Post','incident_id');
    }
}
