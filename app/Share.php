<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    public $timestamps = false;


    protected $table = 'sharepost';
    protected $primaryKey = 'share_id';

    protected $fillable  = ['share_id','post_id','user_id'];

    public function userShared(){
      return $this->belongsTo('App\User','user_id');
    }

    public function postShared(){
      return $this->belongsTo('Post','post_id');
    }

}


