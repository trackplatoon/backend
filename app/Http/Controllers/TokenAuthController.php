<?php

namespace App\Http\Controllers;

use Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User as UserModel; 
use Tymon\JWTAuth\Providers\User\UserInterface;;
use Illuminate\Support\Facades\Hash;
use Authorizer;
use DB;
use Image;
use File;
use App\User;
use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware;


class TokenAuthController extends Controller
{



      // public function __construct()
      //   {
      //       $this->middleware(OAuthMiddleware::class);
      //       $this->middleware(OAuthUserOwnerMiddleware::class);
      //   }

    //
    public function authenticate()
    {
        // Rely on the built-in Input class to get
        // input from whichever format it was sent as.
        try{
          //return 'baal';
        return Authorizer::issueAccessToken();
        }catch(\Exception $e){
          //return $e;
          return Response::json(['access_token'=>null],200);
        }     
       
        }

    public function getAuthenticatedUser()
    {
        try {
 
            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return Response::json(['user_not_found'], 404);
            }
 
        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
 
            return response()->json(['token_expired'], $e->getStatusCode());
 
        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
 
            return response()->json(['token_invalid'], $e->getStatusCode());
 
        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
 
            return response()->json(['token_absent'], $e->getStatusCode());
 
        }
 
        return response()->json(compact('user'));
    }


    public function register(){
            $user = new UserModel;
            $user->email = Input::json()->get('email');
            $user->username = Input::json()->get('username');
            $passwordInput = Input::json()->get('password');
            $password = $password=Hash::make($passwordInput);
            $user->password = $password;
            $address = Input::json()->get('address');
            $street_line_1 = Input::json()->get('streetLine1');
            $street_line_2 = Input::json()->get('streetLine2');
            $city = Input::json()->get('city');
            $zip_code = Input::json()->get('zipCode');
            $region = Input::json()->get('region');
            $country =Input::json()->get('country');
            $birthday =Input::json()->get('birthday');
            $sex =Input::json()->get('sex');
            $religion = Input::json()->get('religion');
            $mobile_number = Input::json()->get('mobileNumber');
            $high_school = Input::json()->get('highSchool');
            $college = Input::json()->get('college');
            $university = Input::json()->get('university');
            $profession = Input::json()->get('profession');
            $blood_group =Input::json()->get('bloodGroup');
            $profile_image_uri = Input::json()->get('profileImage');
            $cover_image_uri = Input::json()->get('coverImage');

            $user->address = $address;

            $user->street_line_1 = $street_line_1;

            $user->street_line_2 = $street_line_2;

            $user->city = $city;

            $user->zip_code = $zip_code;

            $user->region = $region;

            $user->country = $country;

            $user->birthdate = $birthday;

            $user->sex = $sex;

            $user->religion = $religion;

            $user->mobile_number = $mobile_number;

            $user->high_school = $high_school;

            $user->college = $college;

            $user->university = $university;

            $user->profession = $profession;

            $user->blood= $blood_group;
            
            if($profile_image_uri!=null){
                 
                 try{
                    $base_64_string = $profile_image_uri;
                    $pic_name = $this->generateRandomString();
                    $ifp = fopen($pic_name.'.png', "wb"); 

                    $data = explode(',', $base_64_string);
             
                    fwrite($ifp, base64_decode($data[0])); 
                    fclose($ifp);

                    $img = Image::make($pic_name.'.png');

                    // now you are able to resize the instance
                    $img->resize(300, 300);

                    
                    // finally we save the image as a new file
                    $img->save($pic_name.'res.png');


               } catch(Exception $e){
                  return Response::json(['success'=>false],200);
                 }

                 $user->profile_image_uri = $pic_name.'res.png';
                 
                 try{
                    File::delete($pic_name.'png');

                 }catch(Exception $e){
                    return Response::json(['success'=>false],200);
                 }
                 


            }

            else if($cover_image_uri!=null){
                 
                 try{
                    $base_64_string = $cover_image_uri;
                    $pic_name = $this->generateRandomString();
                    $ifp = fopen($pic_name.'.png', "wb"); 

                    $data = explode(',', $base_64_string);
             
                    fwrite($ifp, base64_decode($data[0])); 
                    fclose($ifp);

               } catch(Exception $e){
                   return Response::json(['success'=>false],200);
                 }

                    $user->cover_image_uri = $pic_name.'.png';
            }
            
            // $user->save();
             //Response::json(['success'=>true,'message'=>'user profile updated successfully'],200);



          
          //$userBoolean= DB::table('user')->where('email',Input::json()->get('email'))->get();
          $userBoolean = DB::table('user')->where('email',Input::json()->get('email'))->pluck('email');
          //return $userBoolean;
          if($userBoolean)
            return Response::json(['success'=>false]);
          
          else{
              $user->save();  
          }
          if($user){
            return Response::json(
                  [
                    'success'=>true
                    
                  ]
                  ,200
                );
          }

      return Response::json(['success'=>false]);

      }

    public function generateRandomString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 15; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
      }


      
    }

    

