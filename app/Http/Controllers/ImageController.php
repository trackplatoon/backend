<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Response;
use Image;
use DB;
use SoapClient;
use Illuminate\Support\Facades\Input;
use View;
use Html;
use Validator;
use App\Photos;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use URL;
use File;




class ImageController extends Controller
{
    //

     public function getIndex()
  {
    //Let's load the form view
    return View::make('index');
  }


    public function getImageFromUrl(){

    	// $path = storage_path() . '/' . $filename;

	    // $file = File::get($path);
	    // $type = File::mimeType($path);

	    // //$response = Response::make($file, 200);
	    // //$response->header("Content-Type", $type);

	    // return Response::json('success'=>true,200);

	    $img = Image::make('butterfly.jpg');

		// now you are able to resize the instance
		$img->resize(300, 300);

		
		// finally we save the image as a new file
		$img->save('butterflyresized.jpg');

	    return Response::json(['success'=>true],200);


    }

    public function hardReboot(){
    	//comment_support
    	//comment
    	//follow
    	//notifications
    	//sharepost
    	//support
    	//post
    	//user

     try{
        DB::table('comment_support')->delete();
        DB::table('comment')->delete();
        DB::table('follow')->delete();
        DB::table('notification')->delete();
        DB::table('sharepost')->delete();
        DB::table('support')->delete();
        DB::table('post')->delete();
        DB::table('user')->delete();
     }catch(Exception $e){
     	return Response::json(['success'=>false],200);
     }
       return Response::json(['success'=>true],200);

    }





  public function postIndex()
{
  
  //Let's validate the form first with the rules which areset at the model
  $validation = Validator::make(Input::all(),Photos::$upload_rules);

  //If the validation fails, we redirect the user to theindex page, with the error messages 
  if($validation->fails()) {
    return Redirect::to('/')->withInput()->withErrors($validation);
  }
  else {

    //If the validation passes, we upload the image to thedatabase and process it
    $image = Input::file('image');

    //This is the original uploaded client name of theimage
    $filename = $image->getClientOriginalName();
    //return PATHINFO_FILENAME;
    //Because Symfony API does not provide filename//without extension, we will be using raw PHP here
    $filename = pathinfo($filename, PATHINFO_FILENAME);

    //We should salt and make an url-friendly version of//the filename
    //(In ideal application, you should check the filename//to be unique)
     $fullname = Str::slug(Str::random(8).$filename).'.'.$image->getClientOriginalExtension();

    //We upload the image first to the upload folder, thenget make a thumbnail from the uploaded image
    $upload = $image->move(base_path().'/public/upload_folder/',$fullname);

    //Our model that we've created is named Photo, thislibrary has an alias named Image, don't mix them two!
    //These parameters are related to the image processingclass that we've included, not really related toLaravel
    Image::make( base_path().'/public/upload_folder/'.$fullname)->resize(600,600)->save(base_path().'/public/upload_folder/'.$fullname);

    //If the file is now uploaded, we show an error messageto the user, else we add a new column to the databaseand show the success message
    if($upload) {

      //image is now uploaded, we first need to add columnto the database
      $insert_id = DB::table('photos')->insertGetId(
        array(
          'title' => Input::get('title'),
          'image' => $fullname
        )
      );

      //Now we redirect to the image's permalink
      return Redirect::to(URL::to('snatch/'.$insert_id))->with('success','Your image is uploadedsuccessfully!');
    } else {
      //image cannot be uploaded
      return Redirect::to('/')->withInput()->with('error','Sorry, the image could not beuploaded, please try again later');
    }
  }
}



public function getSnatch($id) {
  //Let's try to find the image from database first
  $image = Photos::find($id);
  //If found, we load the view and pass the image info asparameter, else we redirect to main page with errormessage
  if($image) {
      $images = '';

    foreach (File::allFiles(public_path() . '/upload_folder/') as $file) 
    {
        $filename = $file->getRelativePathName();

        $images .= HTML::image(public_path() . '/upload_folder/'.$filename, $filename);
    }

   return  '<img src="'.$image.'" /><br />';

    //return View::make('permalink')->with('image',$image);


    $dirname = public_path().'/upload_folder';
   $images = glob($dirname."*.png");
   foreach($images as $image) {
    return '<img src="'.$image.'" /><br />';
}

  } else {
    return Redirect::to('/')->with('error','Image not found');
  }
}


    public function getAll(){

      //Let's first take all images with a pagination feature
     // $all_images = DB::table('photos')->orderBy('id','desc')->paginate(6);

      //Then let's load the view with found data and pass thevariable to the view
      return View::make('all_image');
    }

    public function getDelete($id) {
  //Let's first find the image
  $image = Photos::find($id);

 //If there's an image, we will continue to the deletingprocess
  if($image) {
    //First, let's delete the images from FTP
    File::delete(Config::get('image.upload_folder').'/'.$image->image);
    File::delete(Config::get('image.thumb_folder').'/'.$image->image);
  }
}

 
}