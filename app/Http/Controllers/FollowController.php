<?php

namespace App\Http\Controllers;
use Response;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Post as PostModel;   //App\ <--feature of laravel 5.0
use App\User;
use App\Follow as FollowModel;
use App\Notification as NotificationModel;
use Illuminate\Support\Facades\Request;
use JWTAuth;
use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware;
use Authorizer;


class FollowController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(OAuthMiddleware::class);
        $this->middleware(OAuthUserOwnerMiddleware::class);
    }

    public function following(){
         $user_id=Authorizer::getResourceOwnerId(); // the token user_id
         $user= User::find($user_id);// get the user data from database

         if($user){
             $targetUserId = Input::json()->get('userId');
             $followed = Input::json()->get('isFollowed');
             $follow = Db::table('follow')->where('follower_id',$user->user_id)->where('following_id',$targetUserId)->get();

             if($follow){
             	if($followed == 'true' ){
                  return Response::json(['success'=>false,'msg'=>'first'],200);
             	}

             	else if($followed == 'false'){
                  $followModel = FollowModel::where('following_id','=',$targetUserId)->first();
                  $followModel->delete();
                  return Response::json(['success'=>true],200);
             	}
             	else{
                  return Response::json(['success'=>false,'msg'=>'second'],200);
             	}
             }

             else{
             	if($followed == 'false'){
             		return Response::json(['success'=>false,'msg'=>'third'],200);

             	}
             	else if($followed == "true"){
                    $followModel = new FollowModel;
                    $notification = new NotificationModel;
                    $t_user = User::find($targetUserId);
                    //dd($user->user_id);
                    $followModel->follower_id = $user->user_id;
                    $followModel->following_id = $targetUserId;
                    
                    //I made change here
                    if($user->user_id == $targetUserId){                    
                    return Response::json(['success'=>false,'msg'=>'fourth'],200);
                    }
                    //I made change here

                      
                    $notification->sender_id = $user->user_id;
                    $notification->receiver_id = $targetUserId;


                    $notification->post_id = null;
                    $notification->type = 'follow';
                    $strin = $user->username . ' followed you';
                    //dd($user->username, $t_user['username']);
                    $notification->notification_description = $strin;




                    if($followModel->save() && $notification->save()){
                        return Response::json(['successs'=>true,'msg'=>'successfully followed'],200);

                    }
             	}
             	else{
                   return Response::json(['success'=>false,'msg'=>'fifth'],200);
             	}
             }

         }

       }

       public function listoffollowing(){
       	  $user_id=Authorizer::getResourceOwnerId(); // the token user_id
          $user= User::find($user_id);// get the user data from database

          if($user){
             $following = FollowModel::with('following')->where('follower_id',$user->user_id)->get();
             $countFollowing = $following->count();
             return Response::json(['success'=>true,'countFollowing'=>$countFollowing,'users'=>$this->transformCollection($following)],200);
          }
          else{
          	return Response::json(['success'=>false],200);
          }

       }

      public function transformFollowing($users){
        $user = DB::table('user')->where('user_id',$users['following_id'])->first();
        $username = $user->username;
      	return[
            'userId' => $users['following_id'],
            'username' => $username,
            'userProfileImageUrl' => $users['profile_image_uri']
      	];
      }

      public function transformCollection($users)
      {
        return array_map([$this,'transformFollowing'],$users->all());
      }
   
}