<?php

namespace App\Http\Controllers;

use Response;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Support as SupportModel;   //App\ <--feature of laravel 5.0
use App\Post as PostModel;
use App\Comment as CommentModel;
use App\CommentSupport as CommentSupportModel;
use App\User;
use App\Notification as NotificationModel;
use Illuminate\Support\Facades\Request;
use JWTAuth;
use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware;
use Authorizer;


class SupportController extends Controller
{
	public function __construct()
    {
        $this->middleware(OAuthMiddleware::class);
        $this->middleware(OAuthUserOwnerMiddleware::class);
    }

    public function storeSupport(){
    	
    	$user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database

        if($user){
        	$postId =  Input::json()->get('postId');
        	$support = Input::json()->get('isSupported');
          
          $post =  Db::table('support')->where('post_id',$postId)->where('user_id',$user->user_id)->get();            
          
            if($post)
            {
            	//dd("post available");
            	//dd($post);
                if($support == 'true'){
                	return Response::json(['success'=>true],200);
                }
                else if($support == 'false'){
                	    $supportModel = SupportModel::where('post_id','=',$postId)->where('user_id','=', $user->user_id)->first();

                           //delete notification entity as well

	                    $receiver = Db::table('post')->where('post_id',Input::json()->get('postId'))->first();
	                    $sender = $user->user_id;
	
	                    $r = $receiver->user_id;
	
	                    $user_commented = User::find($sender);
	                    $user_notified = Db::table('user')->where('user_id','=',$r)->first();
	
                    $notModel = NotificationModel::where('sender_id','=',$user->user_id)->where('receiver_id','=',$user_notified->user_id)->where('post_id',Input::json()->get('postId'))->first();



                	if($supportModel->delete() && $notModel->delete()){
                	return Response::json(['success'=>true],200);
                    }
                }
                else
                {
                	return Response::json(['success'=>false],501);
                }	
            }

            else
            {
            	//dd("post not available");
            	if($support =="false"){
            		//dd('false');
            		return Response::json(['success'=>true],501);
            	}
            	else if($support == "true"){
            	    // dd('babe');
                   $newSupport = new SupportModel;

                   $notification = new NotificationModel;

                   $newSupport->user_id = $user->user_id;
                   $newSupport->post_id = $postId;

                   
                    $receiver = Db::table('post')->where('post_id',Input::json()->get('postId'))->first();
                    $sender = $user->user_id;

                    $r = $receiver->user_id;

                    $user_commented = User::find($sender);
                    $user_notified = Db::table('user')->where('user_id','=',$r)->first();
                    
                    //I made change here
                    if($user->user_id == $user_notified->user_id){
                       //dd('superbabe');


                       $p = Db::table('post')->where('post_id',$postId)->first();

                       $newSupport->save();
                       $count_sup= $p->count_support+1;
                       $total =    $p->total+1;
                       DB::table('post')->where('post_id',Input::json()->get('postId'))->update(['count_support'=>$count_sup]);
                       DB::table('post')->where('post_id',Input::json()->get('postId'))->update(['total'=>$total]);

                       return Response::json(['success'=>true],200);
                    }
                    //I made change here
                    

                    $notification->sender_id = $user->user_id;
                    $notification->receiver_id = $user_notified->user_id;
                    $notification->post_id = Input::json()->get('postId');
                    $notification->type = 'support';
                    $notification->notification_description = $user->username . ' supported your post';



                   if($newSupport->save() && $notification->save()){
                       $p = Db::table('post')->where('post_id',$postId)->first();

                       $newSupport->save();
                       $count_sup= $p->count_support+1;
                       $total =    $p->total+1;
                       DB::table('post')->where('post_id',Input::json()->get('postId'))->update(['count_support'=>$count_sup]);
                       DB::table('post')->where('post_id',Input::json()->get('postId'))->update(['total'=>$total]);

                     return Response::json(['success'=>true],200);
                  }else{
                     return Response::json(['success'=>false],501);
                  }

            	}else{
            	 return Response::json(['success'=>false],501);
            	}

            }

        }

    }

    public function supportComment(){

        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database

        if($user){
            $commentId = Input::json()->get('commentId');
            $isSupport = Input::json()->get('isSupported');
            $comment = DB::table('comment_support')->where('user_id','=',$user->user_id)->where('comment_id',$commentId)->get();
            

            if($comment){

                if($isSupport == 'true'){
                    return Response::json(['success'=>true],501);


                }else if($isSupport == 'false'){
                    $commentSupportModel = CommentSupportModel::where('comment_id','=',$commentId)->where('user_id','=',$user->user_id)->first();
                    
                    //delete notification entity from the database

                    $key = Db::table('comment')->where('comment_id',Input::json()->get('commentId'))->first();
                    $p = $key->post_id;

                   
                    $receiver = Db::table('post')->where('post_id',$p)->first();
                    $sender = $user->user_id;

                    $r = $receiver->user_id;

                    $user_commented = User::find($sender);
                    $user_notified = Db::table('user')->where('user_id','=',$r)->first();
                    $notModel = NotificationModel::where('sender_id','=',$user->user_id)->where('receiver_id','=',$user_notified->user_id)->where('post_id',$p)->first();


                    //

                    if($commentSupportModel->delete() && $notModel->delete()){
                    return Response::json(['success'=>true],200);
                    }

                }else
                {
                    return Response::json(['success'=>false],501);
                }

            }

            else
            {
                //dd("post not available");
                if($isSupport =="false"){
                    //dd('false');
                    return Response::json(['success'=>true],501);
                }else if($isSupport == 'true'){

                   $key = Db::table('comment')->where('comment_id',Input::json()->get('commentId'))->first();
                   $p = $key->post_id;
                   //dd($p);



                   $newSupportComment = new CommentSupportModel;

                   $notification = new NotificationModel;

                   $newSupportComment->user_id = $user->user_id;
                   $newSupportComment->comment_id = $commentId;

                   $key = Db::table('comment')->where('comment_id',Input::json()->get('commentId'))->first();
                   $p = $key->post_id;

                   
                    $receiver = Db::table('post')->where('post_id',$p)->first();
                    $sender = $user->user_id;

                    $r = $receiver->user_id;

                    $user_commented = User::find($sender);
                    $user_notified = Db::table('user')->where('user_id','=',$r)->first();
                    
                    //I made change here
                     if($user->user_id == $user_notified->user_id){
                       $newSupportComment->save();
                       return Response::json(['success'=>true],200);
                    }
                    //I made change here
                    
                    $notification->sender_id = $user->user_id;
                    $notification->receiver_id = $user_notified->user_id;
                    $notification->post_id = $p;
                    $notification->type = 'support_comment';
                    $notification->notification_description = $user->username . ' supported your comment';


                   if($newSupportComment->save() && $notification->save()){
                     return Response::json(['success'=>true],200);
                  }else{
                     return Response::json(['success'=>false],200);
                  }


                }else{

                    return Response::json(['success'=>false],501);

                }

            }

            
        }

    }

   
}