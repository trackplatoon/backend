<?php

namespace App\Http\Controllers;
use Response;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Share as ShareModel;   //App\ <--feature of laravel 5.0
use App\Notification as NotificationModel;
use App\User;
use Illuminate\Support\Facades\Request;
use JWTAuth;
use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware;
use Authorizer;


class ShareController extends Controller
{
    public function __construct()
    {
        $this->middleware(OAuthMiddleware::class);
        $this->middleware(OAuthUserOwnerMiddleware::class);
    }

    public function sharePost($postId)
    {
    	$user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database
         
        if($user!=null){
        	$share = new ShareModel;
            $notification = new NotificationModel;
        	$share->post_id = $postId;
        	$share->user_id = $user->user_id;

            $post = Db::table('post')->where('post_id',$postId)->first();

            $receiver = Db::table('post')->where('post_id',$postId)->first();
            $sender = $user->user_id;

            $r = $receiver->user_id;

            $user_shared = User::find($sender);
            $user_notified = Db::table('user')->where('user_id','=',$r)->first();
            $not_des = $user_shared['username']+' shared '+$user_notified->username +'\'s post';

            if($user->user_id==$user_notified->user_id){
                $share->save();
                $count_sh= $post->count_share+1;
                $total =    $post->total+1;
                DB::table('post')->where('post_id',$postId)->update(['count_share'=>$count_sh]);
                DB::table('post')->where('post_id',$postId)->update(['total'=>$total]);
                return Response::json(['success'=>true],200);

            }

            $notification->sender_id = $user->user_id;
            $notification->receiver_id = $user_notified->user_id;
            $notification->post_id = $postId;
            $notification->type = 'share';
            $notification->notification_description = $user->username . ' shared your post';

            



        	if($share->save() && $notification->save()){
                 $count_sh= $post->count_share+1;
                 $total =    $post->total+1;
                 DB::table('post')->where('post_id',$postId)->update(['count_share'=>$count_sh]);
                 DB::table('post')->where('post_id',$postId)->update(['total'=>$total]);

        		return Response::json(['success'=>true],200);
        	}else{

                    return Response::json(['success'=>false],200);

             }

        }
        else{
                return Response::json(['success'=>false],200);

        }


    }

}
