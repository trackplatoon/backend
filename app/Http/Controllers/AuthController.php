<?php

namespace App\Http\Controllers;

use Socialite ;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{
	protected $redirectPath = '/home';
    //

    //Redirect the user to the Facebook Authentication page
    public function redirectToProvider()
    {
    	  return \Socialite::driver('facebook')->redirect();
    }



    //Obatin the user information from Facebook
    public function handleProviderCallback()
    {
    	try{

    		$user = \Socialite::driver('facebook')->user();
    		dd($user);

    	}catch(Exception $e){
    		return redirect('auth/facebook');
    	}

       $authUser = $this->findOrCreateUser($user);

       Auth::login($authUser, true);

       return $authUser;

    }

   //Return User if exists; create and return if doesn't
    public function findOrCreateUser($facebookUser)
    {
    	$authUser = User::where('facebook_id', $facebook_id)->first();

    	if($authUser){
    		return $authUser;
    	}

    	return User::create([
              'username' => $facebookUser->name,
              'email' => $facebookUser->email,

    		]);
    }

}
