<?php
namespace App\Http\Controllers;
//require Users/user/laravel/app/Carbon.php;

use Response;
use DB;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Post as PostModel;   //App\ <--feature of laravel 5.0
use App\Follow as FollowModel;
use App\CommentSupport as CommentSupportModel;
use App\User;
use Illuminate\Support\Facades\Request;
use JWTAuth;
use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware;
use Authorizer;
use File;
use Image;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Carbon\Carbon;



class PostController extends Controller
{
    

   public function __construct()
    {
        $this->middleware(OAuthMiddleware::class);
        $this->middleware(OAuthUserOwnerMiddleware::class);
    }




    public function index()
    {
            $posts =  PostModel::with('user')->get();
            $countPost = PostModel::count();
            return Response::json(['success'=>true,'countPost'=>$countPost,'data'=>$this->transformCollection($posts)],200);

        
    }

    public function storePost()
    {
       $user_id=Authorizer::getResourceOwnerId(); // the token user_id
       $user= User::find($user_id);// get the user data from database

       if($user!=null){

          $post = new PostModel;

          $isAnonymous = Input::json()->get("isAnonymous");
          $postImage =   Input::json()->get("postImagePrimary");

          $bool = null;
           if($isAnonymous=='true'){
             $bool = true;
       }else if($isAnonymous=='false'){
          $bool = false;
       }

            if($postImage!=null){
                 
                 try{
                    $base_64_string = $postImage;
                    $pic_name = $this->generateRandomString();
                    $ifp = fopen($pic_name.'.png', "wb"); 

                    $data = explode(',', $base_64_string);
             
                    fwrite($ifp, base64_decode($data[0])); 
                    fclose($ifp);

               } catch(Exception $e){
                  return Response::json(['success'=>false],200);
                 }


                 $post->image_uri = $pic_name.'.png';
                 
                 try{
                    File::delete($pic_name.'png');

                 }catch(Exception $e){
                    return Response::json(['success'=>false],200);
                 }
                 


            }

           $post->user_id = $user->user_id;
           $post->incident_id = Input::json()->get('reportType');
           $post->description = Input::json()->get('postText');
           $post->is_anonymous = $bool;


       $post->save();
       return Response::json(['success'=>true],200);


       }
       else{
        return Response::json(['error'=>'user_not_found'],500);
       }
    }

    public function deletePost()
    {
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database
        //Input::json()->get('postId')
         if($user!=null){
           $post = PostModel::where('post_id','=',Input::json()->get('postId'))->where('user_id',$user->user_id)->first();
             if($post!=null){
             $post->delete();
             return Response::json(['success'=>true],200);
                }
             else{
                Response::json(['error'=>'Not a valid post of the user'],500);
            }
           }
           return Response::json(['error'=>'Cannot delete'],500);
       }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($postId) {
        //$user = JWTAuth::parseToken()->authenticate();
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database
        $id = $postId;
        if($user!=null)
      {
     try{
        $posts = PostModel::with('user','commentsToPost','supportToPosts','incidentToPost')->find($id);
        }catch(NullPointerException $e){
            Response::json(['error'=>'This user does not have the requested post!'],500);
        }
        $countPost = $posts->count();
        $comments = DB::table('comment')->where('post_id',$id)->get();
        return Response::json(['success'=>true,'data'=>$this->transform($posts,$id),'comments'=>$this->transformCollectionForComments($comments)],200);
            
        }
        return Response::json(['error'=>'user_not_found']);
        
    }


    public function getPeopleToDiscover(){
     // dd(Carbon::createFromTimeStamp(strtotime(created_at))->diffForHumans);


      $user_id=Authorizer::getResourceOwnerId(); // the token user_id
      $user= User::find($user_id);// get the user data from database
      $page= \Request::get('page');


      if($user!=null){
          $peop = PostModel::with('user')->select('user_id')->where('user_id','!=',$user->user_id)->
          where('is_anonymous','==',false)->orderBy('total','DESC')->distinct()->paginate(10);
          $people = $peop->forPage($page,5);
          if($people->count()==0){
           return Response::json(['success'=>true,'users'=>$this->transformCollectionForDiscoveredPeople($peop)],200);
          }
          return Response::json(['success'=>true,'users'=>$this->transformCollectionForDiscoveredPeople($peop)],200);

           
        }

          return Response::json(['success'=>false],500);


      }


    

    public function showNewsFeed()
    {
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database
        $page= \Request::get('page');
        $offset = $page;  //take page from input url
        $start=($offset-1)*5;  //make 2 as 10
        $perpage=20;  //make this 10

       if($user!=null){
        

        $postsOfFollowers = PostModel::with('user','commentsToPost','supportToPosts','incidentToPost')->
            orderBy('post_id','DESC')->whereIn('user_id',Db::table('follow')->select('following_id')->
              where('follower_id',$user->user_id))->where('is_anonymous',false)->paginate($perpage/2);

        $restOfThePosts = PostModel::with('user','commentsToPost','supportToPosts','incidentToPost')->
            orderBy('post_id','DESC')->whereNotIn('user_id',Db::table('follow')->
               select('following_id')->where('follower_id',$user->user_id))->orWhere('is_anonymous',true)
                 ->paginate($perpage-count($postsOfFollowers));
        
        
        
         return $posts = $this->merge($postsOfFollowers,$restOfThePosts)->sort();
        // return $posts;
        //return $posts = $postsOfFollowers->merge($restOfThePosts)->forPage(2,5)->toArray();
        
  
        
        //$posts = PostModel::with('user','commentsToPost','supportToPosts','incidentToPost')->orderBy('post_id','DESC')->skip($start)->take($perpage)->get();
        
       $countPost = $posts->count();
        if($countPost==0){
          return Response::json(['success'=>true,'countPost'=>$countPost,'posts'=>$this->transformCollectionFeed($posts)],200);
        }
        return Response::json(['success'=>true,'countPost'=>$countPost,'posts'=>$this->transformCollectionFeed($posts)],200);
      }
      else{
        Response::json(['success'=>false],500);
      }

  }


    public function viewPostByUser($userId)
    {
          
          $user_id=Authorizer::getResourceOwnerId(); // the token user_id
          $user= User::find($user_id);// get  the user data from database
          $true = true;
          $false = false;
          
          $input = $userId;


          // $offset = $page;  //take page from input url
          // $start=($offset-1)*10;  //make 2 as 10
          // $perpage=10;  //make this 10


          //$posts = PostModel::with('user','commentsToPost','supportToPosts','incidentToPost')->find($id);
          //$targetUserId = (object)Input::all();
         
         if($user->user_id==$input){
         $postsByUser = DB::table('post')->where('user_id',$input)->orderBy('post_id','DESC')->paginate(10);
         $countOfPost = DB::table('post')->where('user_id',$input)->count();
       }else{
            $postsByUser = DB::table('post')->where('user_id',$input)->where('is_anonymous',$false)->
             orderBy('post_id','DESC')->paginate(10);
            $countOfPost = DB::table('post')->where('user_id',$input)->where('is_anonymous',$false)->count();
       
       }

         return Response::json(['success'=>true,'countPost'=>$countOfPost,'posts'=>$this->transformCollectionForPostsByUser($postsByUser)],200);

    }


    public function getAnyUserDetails($targetUserId){
      //userId,username,userProfileImageUrl,userCoverImageUrl,isFollowed
      $user_id=Authorizer::getResourceOwnerId(); // the token user_id
      $userdata= User::find($user_id);// get the user data from database
        
      if($userdata){
         // $targetUserId = Input::json()->get('userId');
          $targetUserdata = User::find($targetUserId);

          
          $isFollowing = DB::table('follow')->where('follower_id',$userdata->user_id)->where('following_id',$targetUserId)->first();
         
          if($isFollowing)
          {
            $hasFollowed = true;
          }else{
            $hasFollowed = false;
          }

          return Response::json([
            'success'=>true,
            'userId'=>$targetUserId,
            'username'=>$targetUserdata->username,
            'userProfileImageUrl'=>$targetUserdata->profile_image_uri,
            'userCoverImageUrl'=>$targetUserdata->cover_image_uri,
            'isFollowed'=>$hasFollowed


            ],200);
        }
          return Response::json(['success'=>false],500);

         

    }
    
    public function getUserDetails(){
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $userdata= User::find($user_id);// get the user data from database
        // if($user){
        //   $targetUserId = Input::json()->get('userId');
        //   $userdata = User::find($targetUserId);

        //   //
        //   $isFollowing = DB::table('follow')->where('follower_id',$user->user_id)->where('following_id',$targetUserId)->first();

        //   if($isFollowing)
        //   {
        //     $hasFollowed = true;
        //   }else{
        //     $hasFollowed = false;
        //   }

          

          if($userdata){
          //return  response()->json(compact('userdata'));}
            return Response::json([
              'success'=>true, 
              'userId'=>$userdata->user_id,
              'username'=>$userdata->username,
              'profileImage'=>$userdata->profile_image_uri,
              'coverImage'=>$userdata->profile_image_uri,
              'address'=>$userdata->address,
              'streetLine1'=>$userdata->street_line_1,
              'streetLine2'=>$userdata->street_line_2,
              'city'=>$userdata->city,
              'zipCode'=>$userdata->zip_code,
              'region'=>$userdata->region,
              'country'=>$userdata->country,
              'birthday'=>$userdata->birthdate,
              'sex'=>$userdata->sex,
              'religion'=>$userdata->religion,
              'mobileNumber'=>$userdata->mobile_number,
              'highSchool'=>$userdata->high_school,
              'college'=> $userdata->college, 
              'university'=> $userdata->university,
              'profession'=> $userdata->profession,
              'bloodGroup'=>$userdata->blood
              ],200);
          }
          else
          {
            return Response::json(['success'=>false],501);
          }

        }


    

    public function transform($posts,$id)
    {
        // dd($posts,$id);
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database
        $countComment =DB::table('comment')->where('post_id',$id)->count();
        $countSupport =DB::table('support')->where('post_id',$id)->count() ;
        $countShare=DB::table('sharepost')->where('post_id',$id)->count();
        
        $isSupported = DB::table('support')->where('post_id',$id)->where('user_id',$user->user_id)->first();
        if($isSupported){
          $boolean = true;
        }else{
          $boolean = false;
        }

        if($posts['is_anonymous']==0){
          $isAnonymous = false;
          $u=$posts->user->username;
          $p=$posts->user->profile_image_uri;
          $userId = $posts['user_id'];

        }else{
          $isAnonymous=true;
          $u="Anonymous";
          $p="";
          $userId=0;
        }


    	return[
        'postId'=>$posts['post_id'],
    	  'userId'=>$userId,
        'userProfileImageUrl'=>$p,
        'username'=>$u,
        'reportType'=>$posts['incident_id'],
        'postText'=>$posts['description'],
    	  'postTime'=>Carbon::createFromTimeStamp(strtotime($posts['time_stamp']))->diffForHumans(),
        'countSupport'=>$countSupport,
        'countComment'=>$countComment,
        'countShare'=>$countShare,
        'isSupported'=>$boolean,
        'isAnonymous'=>$isAnonymous,
        'postImagePrimary'=>$posts['image_uri']
    	];
    }

    public function transformPostByUser($postsByUser)
    {
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database
        
        $countComment =DB::table('comment')->where('post_id',$postsByUser->post_id)->count();
        $countSupport =DB::table('support')->where('post_id',$postsByUser->post_id)->where('user_id',$user->user_id)->count();
        $countShare=DB::table('sharepost')->where('post_id',$postsByUser->post_id)->count();
        if($countSupport==0)
        {
            $boolean = false;
        }
        else
        {
            $boolean = true;
        }
        if($postsByUser->is_anonymous==0){
          // $u = Db::table('user')->where('user_id',$postsByUser->user_id)->pluck('username')->first();
          // $p = Db::table('user')->where('user_id',$postsByUser->user_id)->pluck('profile_image_uri')->first();
          $u = User::find($postsByUser->user_id);

          $isAnonymous = false;
          $username = $u->username;
          $profileImage =$u->profile_image_uri;
          $userId = $postsByUser->user_id;

        }else{
          $isAnonymous=true;
          $username = "Anonymous";
          $profileImage = "";
          $userId= 0;

          
        }


        return[
          'postId'=>$postsByUser->post_id,
          'userId'=>$userId,
          'userProfileImageUrl'=>$profileImage,
          'username' =>$username,
          'reportType'=>$postsByUser->incident_id,
          'postText'=>$postsByUser->description,
          'postTime'=>Carbon::createFromTimeStamp(strtotime($postsByUser->time_stamp))->diffForHumans(),
          'countSupport'=>$countSupport,
          'countComment'=>$countComment,
          'countShare'=>$countShare,
          'isSupported'=>$boolean,
          'isAnonymous'=>$postsByUser->is_anonymous,
          'postImagePrimary'=>$postsByUser->image_uri,
          'supportGoal'=>$postsByUser->support_goal
        ];


    }


     public function transformComment($comments)
    {
        //get logged in user from access token
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database

        //check if logged in user supported the comment under transformation
        $supportedComment = CommentSupportModel::where('comment_id','=',$comments->comment_id)->where('user_id','=',$user->user_id)->first();
        if($supportedComment){
            $boolean = true;
        }else{
            $boolean = false;
        }

        $countSupport = Db::table('comment_support')->where('comment_id',$comments->comment_id);
        //dd($countSupport);
        if($countSupport){
          $count = $countSupport->count();
        }else{
          $count = 0;
        }


        $userProfileImageUri = DB::table('user')->where('user_id',$comments->user_id)->first();
        $username = DB::table('user')->where('user_id',$comments->user_id)->pluck('username');
        //$comment
        return[
        'commentId'=>$comments->comment_id,
        'userId'=>$comments->user_id,
        'userProfileImageUri'=>$user->profile_image_uri,
        'username'=>$userProfileImageUri->username,
        'commentText'=>$comments->comment_description,
        'isSupported'=>$boolean,
        'countSupport'=>$count

        ];
    }

    public function transformPeople($people){
        
        return[
        
        'userId'=>$people->user_id,
        'userProfileImageUrl'=>$people->user->profile_image_uri,
        'username'=>$people->user->username
        ];


    }

    public function transformFeed($posts)
    {
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database
        

        $countComment =DB::table('comment')->where('post_id',$posts['post_id'])->count();
        $countSupport =DB::table('support')->where('post_id',$posts['post_id'])->count();
        $countShare = DB::table('sharepost')->where('post_id',$posts['post_id'])->count();
        $count =DB::table('support')->where('post_id',$posts['post_id'])->where('user_id',$user->user_id)->first();


        

        if($count){
          $f = true;
        }else{
          $f = false;
        }

        //$counstShare=5;
        if($countSupport==0)
        {
            $boolean = false;
        }
        else
        {
            $boolean = true;
        }

        if($posts['is_anonymous']==true){
          $uId = "0";
          $username="Anonymous";
          $userProPic = "";
        }else if($posts['is_anonymous']==false){
          $uId = $posts['user_id'];
          $username=$posts->user->username;
          $userProPic =$posts->user->profile_image_uri; 
        }


          $created = new Carbon($posts['time_stamp']);
          $t = $created;
          $now = Carbon::now()->addHours(6);
             //$created->diffForHumans($now);

        return[
        'postId'=>$posts['post_id'],
        'userId'=>$uId,
        'userProfileImageUrl'=>$userProPic,
        'username'=>$username,
        'reportType'=>$posts['incident_id'],
        'postText'=>$posts['description'],
        'postTime'=> $t->diffForHumans($now),
        'countSupport'=>$countSupport,
        'countComment'=>$countComment,
        'countShare'=>$countShare,
        'isSupported'=>$f,
        'isAnonymous'=>$posts->is_anonymous,
        'postImagePrimary'=>$posts['image_uri'],
        'supportGoal'=>$posts->is_anonymous

        ];
    }



    public function transformCollectionForPostsByUser($postsByUser)
    {
        return array_map([$this,'transformPostByUser'], $postsByUser->all());
    }

    public function transformCollection($data)
    {
    	return array_map([$this, 'transform'],$data->all());
    }

    public function transformCollectionForDiscoveredPeople($people){
      return array_map([$this,'transformPeople'],$people->all());
    }

   
    public function transformCollectionForComments($comments){
        return array_map([$this, 'transformComment'],$comments);

    }

    public function transformCollectionFeed($posts)
    {
        return array_map([$this,'transformFeed'],$posts->all());
    }





    
      public function editProfile(){
            
            $user_id=Authorizer::getResourceOwnerId(); // the token user_id
            $user= User::find($user_id);// get the user data from database
            //return $user;
           
            //$username = Input::json()->get('username');
            //$email = Input::json()->get('email');
            if($user!=null){
              
              $address = Input::json()->get('address');
              $street_line_1 = Input::json()->get('streetLine1');
              $street_line_2 = Input::json()->get('streetLine2');
              $city = Input::json()->get('city');
              $zip_code = Input::json()->get('zipCode');
              $region = Input::json()->get('region');
              $country =Input::json()->get('country');
              $birthday =Input::json()->get('birthday');
              $sex =Input::json()->get('sex');
              $religion = Input::json()->get('religion');
              $mobile_number = Input::json()->get('mobileNumber');
              $high_school = Input::json()->get('highSchool');
              $college = Input::json()->get('college');
              $university = Input::json()->get('university');
              $profession = Input::json()->get('profession');
              $blood_group =Input::json()->get('bloodGroup');
              $profile_image_uri = Input::json()->get('profileImage');
              $cover_image_uri = Input::json()->get('coverImage');

              $user->address = $address;

              $user->street_line_1 = $street_line_1;

              $user->street_line_2 = $street_line_2;

              $user->city = $city;
              $user->zip_code = $zip_code;

              $user->region = $region;

              $user->country = $country;

              $user->birthdate = $birthday;

              $user->sex = $sex;

              $user->religion = $religion;

              $user->mobile_number = $mobile_number;

              $user->high_school = $high_school;

              $user->college = $college;

              $user->university = $university;

              $user->profession = $profession;

              $user->blood = $blood_group;


               if($profile_image_uri!=null){
                 
                 try{

                    $pic_name = $user->profile_image_uri;

                    File::delete($pic_name);

                    $base_64_string = $profile_image_uri;
                    $pic_name = $this->generateRandomString();
                    $ifp = fopen($pic_name.'.png', "wb"); 

                    $data = explode(',', $base_64_string);
             
                    fwrite($ifp, base64_decode($data[0])); 
                    fclose($ifp);
                    $img = Image::make($pic_name.'.png');

                    // now you are able to resize the instance
                    $img->resize(300, 300);

                    
                    // finally we save the image as a new file
                    $img->save($pic_name.'res.png');


               } catch(Exception $e){
                  return Response::json(['success'=>false],200);
                 }

                 $user->profile_image_uri = $pic_name.'res.png';
            }

            else if($cover_image_uri!=null){
                 
                 try{
                    $base_64_string = $cover_image_uri;
                    $pic_name = $this->generateRandomString();
                    $ifp = fopen($pic_name.'.png', "wb"); 

                    $data = explode(',', $base_64_string);
             
                    fwrite($ifp, base64_decode($data[0])); 
                    fclose($ifp);

               } catch(Exception $e){
                    return Response::json(['success'=>false],200);
                 }

                    $user->cover_image_uri = $pic_name.'.png';
            }
            
             $user->save();
             return Response::json(['success'=>true],200);

            }

            else{
             return Response::json(['success'=>false],500);
            }

            
      }

      public function generateRandomString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 15; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
      }

  public function merge(LengthAwarePaginator $collection1, LengthAwarePaginator $collection2)
    {
        $total = $collection1->total() + $collection2->total();

        $perPage = $collection1->perPage() + $collection2->perPage();

        $items = array_merge($collection1->items(), $collection2->items());

        $paginator = new LengthAwarePaginator($items, $total, $perPage);

        return $paginator;
    }


}