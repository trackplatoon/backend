<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware;
use Authorizer;
use App\User ;
use App\Notification as NotificationModel;
use Response;
use DB;
use Illuminate\Support\Facades\Input;



class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware(OAuthMiddleware::class);
        $this->middleware(OAuthUserOwnerMiddleware::class);
    }

    public function getnotifications(){
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database

        $notification = NotificationModel::where('receiver_id','=',$user->user_id)->get();
        $countUnread = NotificationModel::where('receiver_id','=',$user->user_id)->where('is_read','=',false)->count();
        if($notification){

            return Response::json([
            'success'=>true,
            'countUnread'=>$countUnread,
            'notifications'=>$this->transformCollection($notification)
            ],200);


        }else{
            Response::json(['success'=>false,'message'=>'no notifications for you'],200);
        }
        




    }

    public function readNotification(){
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database

        $notification = NotificationModel::find(Input::json()->get('notificationId'));

        if($notification->is_read == true){
            return Response::json(['success'=>true],200);
        }
        
        DB::table('notification')->where('notification_id',Input::json()->get('notificationId'))->update(['is_read'=>true]);

        if($notification->save()){
            return Response::json(['success'=>true],200);
        }
        else{
            return Response::json(['success'=>false],200);
        }

    }

    public function getUnreadNotification(){

        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database

        $notification = NotificationModel::where('receiver_id',$user->user_id)->where('is_read',0)->get();

        if($notification){
            return Response::json(['userId'=>$user->user_id,'userProfileImageUrl'=>$user->profile_image_uri,"unreadNotifications"=>$notification->count()],200);
        }else{
            return Response::json(["success"=>false],500);
        }


    }

    public function transform($notification){
        
        return[
          'notificationId' => $notification['notification_id'],
          'senderId'=>$notification['sender_id'],
          'type'=>$notification['type'],
          'postId'=>$notification['post_id'],
          'notificationText'=>$notification['notification_description'],
          'isRead'=>$notification['is_read'],

        ];
    }

    public function transformCollection($notification)
    {
        return array_map([$this,'transform'], $notification->all());
    }
}
