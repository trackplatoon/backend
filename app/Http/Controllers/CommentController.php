<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Response;
use DB;
use Illuminate\Support\Facades\Input;
use App\Comment as CommentModel;   //App\ <--feature of laravel 5.0
use App\Notification as NotificationModel;
use App\User;
use App\Post as PostModel;
use Auth;

use LucaDegasperi\OAuth2Server\Middleware\OAuthMiddleware;
use LucaDegasperi\OAuth2Server\Middleware\OAuthUserOwnerMiddleware;
use Authorizer;


class CommentController extends Controller
{

     public function __construct()
    {
        $this->middleware(OAuthMiddleware::class);
        $this->middleware(OAuthUserOwnerMiddleware::class);
    }

    //
    public function storeComment()
    {
        $user_id=Authorizer::getResourceOwnerId(); // the token user_id
        $user= User::find($user_id);// get the user data from database

    	$comment = new CommentModel;
        $notification = new NotificationModel;

        //$post = new PostModel;

    	$comment->user_id = $user->user_id;
    	$comment->post_id = Input::json()->get('postId');
    	$comment->comment_description = Input::json()->get('commentText');

        $post = Db::table('post')->where('post_id',Input::json()->get('postId'))->first();

        
        
        //$post->count_comment = $post->count_comment+1;


        $receiver = Db::table('post')->where('post_id',Input::json()->get('postId'))->first();
        $sender = $user->user_id;

        $r = $receiver->user_id;

        $user_commented = User::find($sender);
        $user_notified = Db::table('user')->where('user_id','=',$r)->first();
        $not_des = $user_commented['username']+' commented on '+$user_notified->username +'\'s post';
        
        //I made change here
        if($user->user_id == $user_notified->user_id){
         $comment->save();
         $count_co = $post->count_comment+1;
         $total =    $post->total+1;
         DB::table('post')->where('post_id',Input::json('postId'))->update(['count_comment'=>$count_co]);
         DB::table('post')->where('post_id',Input::json('postId'))->update(['total'=>$total]);


         return Response::json(['success'=>true],200);
        }
        //I made change here

        $notification->sender_id = $user->user_id;
        $notification->receiver_id = $user_notified->user_id;
        $notification->post_id = Input::json()->get('postId');
        $notification->type = 'comment';
        $notification->notification_description = $user->username . ' commented on your post';

        if($comment->save() && $notification->save()){
            
             $count_co = $post->count_comment+1;
             $total =    $post->total+1;
             DB::table('post')->where('post_id',Input::json('postId'))->update(['count_comment'=>$count_co]);
             DB::table('post')->where('post_id',Input::json('postId'))->update(['total'=>$total]);

            return Response::json(['success'=>true],200);

        }
        else{
            return Response::json(['error'=>'cannot save comment'],500);
        }


    }
}