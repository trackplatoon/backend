<?php
//feed
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/',array('as'=>'index_page','uses'=>'ImageController@getIndex'));
//This is for the post event of the index.page
Route::post('/',array('as'=>'index_page_post','before' =>'csrf', 'uses'=>'ImageController@postIndex'));

//This is to show the image's permalink on our website
Route::get('snatch/{id}',
  array('as'=>'get_image_information',
  'uses'=>'ImageController@getSnatch'))
  ->where('id', '[0-9]+');


//This is to show the image's permalink on our website
Route::get('snatch/{id}',
  array('as'=>'get_image_information',
  'uses'=>'ImageController@getSnatch'))
  ->where('id', '[0-9]+');

  //This route is to show all images.
Route::get('all','ImageController@getAll');

Route::get('delete/{id}', array
('as'=>'delete_image','uses'=>
'ImageController@getDelete'))
->where('id', '[0-9]+');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/



Route::group(['middleware' => ['web']], function () {
    //
   Route::get('auth/facebook', 'AuthController@redirectToProvider');
   Route::get('auth/facebook/callback', 'AuthController@handleProviderCallback');
   Route::get('home', array('as' => 'home', 'uses' => function(){
    return view('home');
    }));
   
   Route::post('register', 'TokenAuthController@register');
   Route::post('login', 'TokenAuthController@authenticate');
   Route::get('authenticate/user', 'TokenAuthController@getAuthenticatedUser');

   Route::post('hardreboot','ImageController@hardReboot');
   //Route::post('image','ImageController@getImageFromUrl');
   Route::post('weirdo','ImageController@weirdo');
   

});

Route::group(['before' => 'oauth'], function()  //all done and checked
{
   Route::get('posts/all','PostController@showNewsFeed');   //done and checked DONE
   Route::get('posts/{postId}','PostController@show'); //done and checked 
   Route::get('user/{userId}/posts','PostController@viewpostbyuser'); //done and checked DONE
   Route::post('posts','PostController@storePost');  //done and checked
   Route::post('deletepost','PostController@deletePost'); //done and checked
   Route::post('comment','CommentController@storeComment'); //done and checked
   Route::post('supportpost','SupportController@storeSupport');
   Route::get('user/{userId}','PostController@getAnyUserDetails');
   Route::post('follow','FollowController@following');
   Route::get('follow','FollowController@listoffollowing');
   Route::get('notifications','NotificationController@getnotifications');
   Route::post('supportcomment','SupportController@supportComment');
   Route::post('notifications/read','NotificationController@readNotification');
   Route::post('share/{postId}','ShareController@sharePost');
   Route::post('userinfo','PostController@editProfile');
   Route::get('userinfo','PostController@getUserDetails');
   Route::get('dashboard','NotificationController@getUnreadNotification');
   Route::get('discover','PostController@getPeopleToDiscover');

});

//Route::resource('post','PostController');
Route::resource('user','User');