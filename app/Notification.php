<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    //
    protected $table = 'notification';
    protected $primaryKey = 'notification_id';
    public $timestamps = false;
    protected $fillable  = ['type','sender_id','receiver_id','post_id','notification_description','is_read'];

    public function sender(){
    	$this->belongsTo('App\User','user_id');
    }

    public function receiver(){
    	$this->belongsTo('App\User','user_id');
    }

    public function postedOn(){
    	$this->belongsTo('App\Post','post_id');
    }

}
