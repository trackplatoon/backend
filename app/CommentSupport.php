<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentSupport extends Model
{
    //
    public $timestamps = false;


    protected $table = 'comment_support';
    protected $primaryKey = 'comment_support_id';

    protected $fillable  = ['comment_id','user_id'];

    //A single support belongs to a single comment
    public function commentSupported()
    {
    	return $this->belongsTo('Comment','comment_id');
    }

    //A single support belongs to a single user
    public function userSupported()
    {
    	return $this->belongsTo('User','user_id');
    }

}
