<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    //
    protected $table = 'follow';
    protected $primaryKey = 'follow_id';
    public $timestamps = false;
    protected $fillable = ['follower_id','following_id'];

    public function followers()
    {
    	return $this->belongsTo('App\User','follower_id');

    }

    public function following()
    {
    	return $this->belongsTo('App\User','following_id');
    }

}
