<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    public $timestamps = false;


    protected $table = 'comment';
    protected $primaryKey = 'comment_id';

    protected $fillable  = ['comment_id','user_id','comment_description',
    'post_id','is_support'];


    //A comment belongs to a single user
    public function userCommented()
    {
    	return $this->belongsTo('App\User','user_id');
    }

    //A comment belongs to a single post
    public function postCommentedOn()
    {
    	return $this->belongsTo('Post','post_id');
    }


}
